
import java.util.Scanner;

//Из текста удалить все слова заданной длины, начинающиеся на согласную букву.
public class Lab7 {
    
    public static boolean isSog (String s, int k){
        boolean result = false;
        char[] alph = {'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'};
        
        for (int j = 0; j < alph.length; j++){
            if ((s.charAt(0) == (char) alph[j]) && (s.length() == k)) {
                result = true;
                break;
            }
        }
        return result;
    }    
    
    public static void main(String[] args) {
        
        Scanner sc1 = new Scanner(System.in);
        System.out.println( "Введите строку:");
        String str = sc1.nextLine();
        System.out.println( "Введите количество букв в искомом слове (k):");
        int k = sc1.nextInt();
        
        
        String[] s = str.split(" ");
        String newStr = new String();
        
        
       
        for ( int i = 0; i < s.length; i++) {
            System.out.println((i+1) + " слово: " + s[i]);
            
            if (!(isSog(s[i], k))){
                System.out.println("Это походящее слово");
                newStr = newStr + s[i] + " ";
                
            } 
                       
            
        }
        
        System.out.println("Новая строка: " + newStr);    
        
    }    
       
}

